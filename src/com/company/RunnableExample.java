package com.company;


public class RunnableExample implements Runnable {


    private String TAG = RunnableExample.class.getSimpleName();

    public int count = 0;

    @Override
    public void run() {
        System.out.println(TAG + " is starting");

        try {
            while (count != 5) {
                Thread.sleep(500);

                count++;
            }
        } catch (InterruptedException e) {
            System.out.println(TAG + " interrupted");
        }

        System.out.println(TAG + " terminating");

    }


    public static void main(String[] args) {
        RunnableExample instance = new RunnableExample();
        Thread thread = new Thread(instance);
        thread.start();

        while (instance.count != 5) {
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


    }
}
